///////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2021, STEREOLABS.
//
// All rights reserved.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
///////////////////////////////////////////////////////////////////////////

/*********************************************************************************
 ** This sample demonstrates how to capture and process the streaming video feed **
 ** provided by an application that uses the ZED SDK with streaming enabled.     **
 **********************************************************************************/

// Standard includes
#include <stdio.h>
#include <string.h>
#include <unordered_map>

// ZED include
#include <sl_mc/MultiCameraHandler.hpp>

// OpenCV include (for display)
#include <opencv2/opencv.hpp>

//ZED IOT
#include <sl_iot/IoTCloud.hpp>

#include "ConfManager.hpp"

#ifdef OPENGL_DISPLAY
#include "GLViewer.hpp"
#endif

using namespace sl;
using namespace sl_iot;
using json = sl_iot::json;
#define WITH_DISPLAY 0

// disable for debuging for example
#define USE_THREADS 0


/************     GLOBAL VARIABLES    *************/
TARGET topic_target = TARGET::LOCAL_NETWORK;
std::string objects_topic_name = "/multicam/skeleton";
/**************************************************/

// Using std and sl namespaces
cv::Mat slMat2cvMat(sl::Mat &input)
{
    int cv_type = -1;
    switch (input.getDataType())
    {
    case sl::MAT_TYPE::F32_C1:
        cv_type = CV_32FC1;
        break;
    case sl::MAT_TYPE::F32_C2:
        cv_type = CV_32FC2;
        break;
    case sl::MAT_TYPE::F32_C3:
        cv_type = CV_32FC3;
        break;
    case sl::MAT_TYPE::F32_C4:
        cv_type = CV_32FC4;
        break;
    case sl::MAT_TYPE::U8_C1:
        cv_type = CV_8UC1;
        break;
    case sl::MAT_TYPE::U8_C2:
        cv_type = CV_8UC2;
        break;
    case sl::MAT_TYPE::U8_C3:
        cv_type = CV_8UC3;
        break;
    case sl::MAT_TYPE::U8_C4:
        cv_type = CV_8UC4;
        break;
    default:
        break;
    }
    // Since cv::Mat data requires a uchar* pointer, we get the uchar1 pointer from sl::Mat (getPtr<T>())
    // cv::Mat and sl::Mat will share a single memory structure
    return cv::Mat(input.getHeight(), input.getWidth(), cv_type, input.getPtr<sl::uchar1>(sl::MEM::CPU));
}



/**
 * @brief slObjectsToJson : convert a sl::Object into a json file
 * @param sl_obj : sl::Object current
 * @param json_io : reference to json object that will be filled.
 */
void convertObjectsToJson(sl::Objects& sl_obj, json& json_io) {
    json_io.clear();

    json my_camera_objects = json::object();
    my_camera_objects["timestamp"] = sl_obj.timestamp.getNanoseconds();
    my_camera_objects["camera_id"] = -1;

    // Objects
    for (auto & obj_data_it : sl_obj.object_list) {
        if (std::isfinite(obj_data_it.position.x) && std::isfinite(obj_data_it.position.y) && std::isfinite(obj_data_it.position.z))
        {
            json my_object = json::object();

            //gle data
            my_object["class"] =obj_data_it.label;

            my_object["bbox_2D"].push_back(obj_data_it.bounding_box_2d[0].x);
            my_object["bbox_2D"].push_back(obj_data_it.bounding_box_2d[0].y);
            float width = obj_data_it.bounding_box_2d[2].x - obj_data_it.bounding_box_2d[0].x;
            float height = obj_data_it.bounding_box_2d[2].y - obj_data_it.bounding_box_2d[0].y;
            my_object["bbox_2D"].push_back(width);
            my_object["bbox_2D"].push_back(height);

            my_object["confidence"] = obj_data_it.confidence;
            my_object["object_id"] = obj_data_it.id;
            my_object["tracking_state"] = obj_data_it.tracking_state;
            my_object["action_state"] = obj_data_it.action_state;
            my_object["3D available"] = true;

            //3D data
            my_object["position_3D"]["x"] = obj_data_it.position.x;
            my_object["position_3D"]["y"] = obj_data_it.position.y;
            my_object["position_3D"]["z"] = obj_data_it.position.z;

            my_object["velocity_3D"]["x"] = obj_data_it.velocity.x;
            my_object["velocity_3D"]["y"] = obj_data_it.velocity.y;
            my_object["velocity_3D"]["z"] = obj_data_it.velocity.z;

            my_object["dimensions_3D"]["width"] = obj_data_it.dimensions.x;
            my_object["dimensions_3D"]["height"] = obj_data_it.dimensions.y;
            my_object["dimensions_3D"]["length"] = obj_data_it.dimensions.z;

            //bbox_3D
            for (int i = 0; i < obj_data_it.bounding_box.size(); i++){
                json my_point = json::object();
                my_point["x"] = obj_data_it.bounding_box[i].x;
                my_point["y"] = obj_data_it.bounding_box[i].y;
                my_point["z"] = obj_data_it.bounding_box[i].z;
                my_object["bbox_3D"].push_back(my_point);
            }

            for (int i = 0; i < 6; i++){
                my_object["position_covariance_3D"].push_back(obj_data_it.position_covariance[i]);
            }

            //keypoint_3D
            if (obj_data_it.keypoint.size()>0){
                for (int i = 0; i < obj_data_it.keypoint.size(); i++){
                    json my_keypoint = json::object();
                    my_keypoint["x"] = obj_data_it.keypoint[i].x;
                    my_keypoint["y"] = obj_data_it.keypoint[i].y;
                    my_keypoint["z"] = obj_data_it.keypoint[i].z;
                    my_object["keypoint_3D"].push_back(my_keypoint);
                }
            }

            //keypoint_2D
            if (obj_data_it.keypoint_2d.size()>0){
                for (int i = 0; i < obj_data_it.keypoint_2d.size(); i++){
                    json my_keypoint = json::object();
                    my_keypoint["x"] = obj_data_it.keypoint_2d[i].x;
                    my_keypoint["y"] = obj_data_it.keypoint_2d[i].y;
                    my_object["keypoint_2D"].push_back(my_keypoint);
                }
            }
            my_camera_objects["object_list"].push_back(my_object);
        }
    }
    json_io["Objects"] = my_camera_objects;
}


std::unordered_map<int, sl::Transform> readCameraRelocFile(std::string filename, sl::COORDINATE_SYSTEM coord_sys, sl::UNIT unit)
{
    std::unordered_map<int, sl::Transform> cam_conf;
    CSimpleIniA confReader;
    confReader.SetUnicode();
    auto rc = confReader.LoadFile(filename.c_str());
    if (rc < 0)
    {
        std::cout << "Error open conf file \n";
        return cam_conf;
    }

    auto tr_im2user = sl::getCoordinateTransformConversion4f(sl::COORDINATE_SYSTEM::IMAGE, coord_sys);

    std::list<CSimpleIniA::Entry> vec;
    confReader.GetAllSections(vec);
    std::string cam_name;
    sl::float3 rv;
    float ratio_ = sl::getUnitScale(sl::UNIT::MILLIMETER, unit);
    for (int c = 0; c < vec.size(); c++)
    {
        sl::Transform pose;
        cam_name = "camera" + std::to_string(c);
        int sn = std::atoi(confReader.GetValue(cam_name.c_str(), "sn", "0.f"));
        pose.tx = std::atof(confReader.GetValue(cam_name.c_str(), "tx", "0.f")) * ratio_;
        pose.ty = std::atof(confReader.GetValue(cam_name.c_str(), "ty", "0.f")) * ratio_;
        pose.tz = std::atof(confReader.GetValue(cam_name.c_str(), "tz", "0.f")) * ratio_;
        rv[0] = std::atof(confReader.GetValue(cam_name.c_str(), "rx", "0.f"));
        rv[1] = std::atof(confReader.GetValue(cam_name.c_str(), "ry", "0.f"));
        rv[2] = std::atof(confReader.GetValue(cam_name.c_str(), "rz", "0.f"));
        pose.setRotationVector(rv);

        cam_conf[sn] = tr_im2user * pose * sl::Matrix4f::inverse(tr_im2user);
    }
    return cam_conf;
}


std::map<int, sl::String> generates_sn_ip_map(std::unordered_map<int, sl::Transform> pose_map){

    std::map<int, sl::String> ip_map;
    ip_map[27000102] = "192.168.1.17"; //E-Nano
    ip_map[27355959] = "192.168.2.148"; //N-X2
    ip_map[28625780] = "192.168.2.150"; //S-Nano
    ip_map[20491383] = "192.168.2.149"; //W-X2

    for (auto& it : pose_map){
        if(ip_map.find(it.first)==ip_map.end()){
            std::cout << "ERROR: no IP found for sn " << it.first << std::endl;
        }
    }
    return ip_map;
}



int main(int argc, char **argv)
{


    if (argc < 3)
    {
        std::cout << "ERROR: Need .rloc file " << std::endl;
        std::cout << "Usage : ./MultiCameraAPI_test $PATH_TO_.rloc $INPUT_MODE  $SVO_folder(if use_svo)" << std::endl;
        std::cout << "INPUT_MODE in ['use_svo', use_stream', 'use_id']" << std::endl;
        return 1;
    }


    const bool use_svo = std::string(argv[2]).compare("use_svo") == 0;
    const bool use_stream = std::string(argv[2]).compare("use_stream") == 0;
    const bool use_id = std::string(argv[2]).compare("use_id") == 0;

    if (!use_svo && !use_stream && !use_id){
        std::cout << "ERROR: unknown input mode "<< argv[2] << std::endl;
        std::cout << "Usage : ./MultiCameraAPI_test $PATH_TO_.rloc $INPUT_MODE  $SVO_folder(if use_svo)" << std::endl;
        std::cout << "INPUT_MODE in ['use_svo', use_stream', 'use_id']" << std::endl;
        return 1;
    }


    std::string folder_name;
    if (use_svo)
    {
        if (argc == 4){
            folder_name = argv[3];
            std::cout << "use svo in " << folder_name << std::endl;;
        }
        else{
            std::cout << "ERROR: unknown svo folder" << std::endl;
            std::cout << "Usage : ./MultiCameraAPI_test $PATH_TO_.rloc $INPUT_MODE  $SVO_folder(if use_svo)" << std::endl;
            std::cout << "INPUT_MODE in ['use_svo', use_stream', 'use_id']" << std::endl;
        }
    }

    auto pose_map = readCameraRelocFile(argv[1], sl::COORDINATE_SYSTEM::IMAGE, sl::UNIT::METER);
    if (pose_map.size() == 0){
        std::cout << "empty calibration file. No camera to be opened found" << std::endl;
        return 0;
    }
    for (auto it : pose_map)        std::cout << "Serial " << it.first << "\n" << it.second.getInfos() << "\n";


    std::map<int, sl::String> ip_map;
    if(use_stream){
        ip_map = generates_sn_ip_map(pose_map);
    }

    // Init CMP app
    const char * application_token = "myApp";

    STATUS_CODE status_iot = IoTCloud::init(application_token);
    if (status_iot != STATUS_CODE::SUCCESS) {
        std::cout << "Initiliazation error " << status_iot << std::endl;
        exit(EXIT_FAILURE);
    }

    //Set log level
    IoTCloud::setLogLevelThreshold(LOG_LEVEL::INFO,LOG_LEVEL::INFO);

    //Send a log
    IoTCloud::log("Initialization succeeded",LOG_LEVEL::INFO);


    sl::MultiCameraHandler multi_camera;

    // init multi camera
    sl::InitMultiCameraParameter init_multi_cam_parameters;
    init_multi_cam_parameters.max_input_fps = 15.f;
    multi_camera.init(init_multi_cam_parameters);
    std::cout << "multicam init" << std::endl;

    std::vector<sl::CameraIdentifier> camera_identifiers;

    // WARNING : in SVO mode, enable all modules before addCamera
    // because some modules may take several time to initialize. It may 
    // create a latency in some cameras.
    sl::ObjectDetectionFusionParameters OD_fusion_init_params;
    OD_fusion_init_params.detection_model = sl::DETECTION_MODEL::HUMAN_BODY_ACCURATE;
    multi_camera.enableObjectDetectionFusion(OD_fusion_init_params);
    std::cout << "od enabled" << std::endl;

	// add cameras
	int z = 0;
    for (auto& it : pose_map)
    {
        std::cout << "use_stream " <<  use_stream << std::endl;
        std::cout << "use_svo " <<  use_svo << std::endl;
        if (use_svo)
        {
            std::string svo_name = (folder_name + "/SVO_" + std::to_string(it.first) + ".svo");
            std::cout << "Open " << svo_name << "\n";
			sl::InputType input;
            input.setFromSVOFile(svo_name.c_str()); //setFromStream
			sl::CameraIdentifier uuid;
            
			multi_camera.addCamera(input, uuid, it.second);
            std::cout << "svo " << z << " has sn " << uuid.sn << " ; pose passed has sn " << it.first << "\n";

			camera_identifiers.push_back(uuid);
        }
        else if(use_stream){
            sl::InputType input;
            //sl::String sender_ip = ip_map[it.first]; //it.first = sn
		auto sender_ip_it = ip_map.find(it.first);
		if (sender_ip_it != ip_map.end())
{
		sl::String sender_ip = sender_ip_it->second;
            std::cout << "adding camera ip " << sender_ip << std::endl;

            input.setFromStream(sender_ip);

            sl::CameraIdentifier uuid;
            multi_camera.addCamera(input, uuid, it.second);
            camera_identifiers.push_back(uuid);
}
        }
		else
		{
			sl::InputType input;
			input.setFromCameraID(z);
			sl::CameraIdentifier uuid;
			multi_camera.addCamera(input, uuid, it.second);
			camera_identifiers.push_back(uuid);
		}
		z++;
    }

#ifdef OPENGL_DISPLAY
	// Create OpenGL Viewer
	GLViewer viewer;

	viewer.init(argc, argv, false);

    bool is_available = false;
#endif

    //std::this_thread::sleep_for(std::chrono::seconds(1));   
	sl::RuntimeMultiCameraParameters rt_m;
    int n = 0;
    //while (n++ < 879)
    while (1)
    {
		if (multi_camera.grabAll(rt_m) != sl::ERROR_CODE::SUCCESS){
            std::cout << "grab all failed " << std::endl;
			break;
        }
		else
		{//std::cout << "grab new frame at  " <<n++<< std::endl;
sl::Objects objs;
			multi_camera.retrieveFusedObjects(objs);
        
#ifdef OPENGL_DISPLAY
                        //Update GL View
                        if (is_available) viewer.updateData(objs.object_list);
                        is_available = viewer.isAvailable();
#endif

		if(objs.object_list.size() > 0)
            {
            std::cout << "objs " << objs.object_list.size() << std::endl;
                        //publish obj on MQTT topic
                        json object_data_js;
                        convertObjectsToJson(objs, object_data_js);

                        IoTCloud::publishOnMqttTopic(objects_topic_name, object_data_js, topic_target);
                    //    std::cout << "Sent mqtt message on topic " << topic_target << " + " << objects_topic_name << std::endl;
                    //    std::cout << "object_data_js" << object_data_js << std::endl;

            }
		}
	//sl::sleep_ms(10);
    }

    viewer.exit();
    std::cout << "Leave Main looop " << std::endl;
    

	std::cout << "Detach all thread " << std::endl;
	for (auto &it : camera_identifiers)
		multi_camera.removeCamera(it);

    multi_camera.disableObjectDetectionFusion();
    std::cout << "Disable OD Fusion" << std::endl;

    multi_camera.close();
    std::cout << "Close Multi" << std::endl;

    return EXIT_SUCCESS;
}
